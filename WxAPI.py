import requests
import json
import time
from urllib.parse import urlparse

def Update():
    Path = "C:\/Program Files\/WxMesgNet\/WxData\/RxFiles"
    ALERTS = requests.get("https://api.weather.gov/alerts/active")
    URLS =[]

    for ID in ALERTS.json()['features']:
        URLS.append(ID['id'])

    for URL in URLS:
        name = URL.rsplit('/', 1)[-1]
        file = open(Path+"\/" + name+ ".xml", 'w')
        file.write(requests.get(URL, headers={"accept":"application/cap+xml"}).text)
        file.close()
        print("[+] DONE: " +name)
Update()